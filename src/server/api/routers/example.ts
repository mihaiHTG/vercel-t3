import { z } from "zod";

import {
  createTRPCRouter,
  protectedProcedure,
  publicProcedure,
} from "~/server/api/trpc";

export const exampleRouter = createTRPCRouter({
  hello: publicProcedure
    .input(z.object({ text: z.string() }))
    .query(({ input }) => {
      return {
        greeting: `Hello ${input.text}`,
      };
    }),

  getSecretMessage: protectedProcedure.query(() => {
    return "you can now see this secret message!";
  }),

  getTimedMessage: publicProcedure.query(() => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve("this message was delayed by 1 second");
      }, 1000);
    });
  }),
  getTriggeredTimedMessage: publicProcedure.mutation(() => {
    return new Promise((resolve) => {
      setTimeout(() => {
        console.log("triggered");
        resolve("this message was delayed by 10 second");
        console.log("resolved");
      }, 10000);
    });
  }),
});
